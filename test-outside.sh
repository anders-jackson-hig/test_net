#!/bin/bash
#
# Testing access to an IPv6 machine from distance
#
# Date      Name            What
# -------- ---------------- ---------------
# 2016xxyy Anders Jackson   First version
# 20170125 Anders Jackson   Some clean up
# 20170215 Anders Jackson   Removed commands not installed
# 20180214 Anders Jackson   Cleaned up some testing of command exists
# 20210629 Anders Jackson   Correct an spelling error and change hopp
#

# Get IPv6 address from command line.
if [ $# -ne 1 ]; then
    echo -e 2>1 "usage: `basename $0` server-address"
    echo -e 2>1 "   server-address is IPv6 or DNS-name"
    exit 1
fi

SERVER="$1"

# Look up with getent(1) so we get IP address and name
# If IPv6 address, get IPv6 and name
# If name, get IPv6 and name
# If no IPv6 address or name exists, returns empty string.
echo -e "Server IPv6: $SERVER"

TMP=`getent hosts "$SERVER"`
if [ ! -z "$TMP" ]; then
    echo -e "Värde från getent(1): $TMP"
    SERVER=`echo "$TMP" | cut -d " " -f1`
else
    echo -e "Inget från getent hosts $SERVER"
fi

echo -e "\nTest ICMP6 ECHO"
echo ping6 -c 3 -n $SERVER
ping6 -c 3 -n $SERVER

hopp=20
echo -e "\nTest routing IPv6 (at least one should exist)"
if [ ! -z `which traceroute6` ]; then
    echo "traceroute6 -m $hopp -p 80 $SERVER"
    traceroute6 -m $hopp -p 80 $SERVER
else
    echo -e "Inte installerat: traceroute6"
fi

if [ ! -z `which tracepath` ]; then
    echo "tracepath -6 -b -m $hopp -p 80 $SERVER"
    tracepath -6 -b -m hopp -p 80 $SERVER
else
    echo -e "Inte installerat: tracepath"
fi

if [ ! -z `which rltraceroute6` ]; then
    echo "rltraceroute6 -m $hopp $SERVER"
    rltraceroute6 -m $hopp $SERVER
else
    echo -e "Inte installerat: rltraceroute6"
fi

if [ ! -z `which tcptraceroute6` ]; then
    echo "tcptraceroute6 -m $hopp $SERVER"
    tcptraceroute6 -m $hopp $SERVER
else
    echo -e "Inte installerat: tcptraceroute6"
fi

echo -e "\nTest open ports (nmap)"
echo nmap -6 -AT4 $SERVER
nmap -6 -AT4 $SERVER

# Lokalt, innefrån
#echo
#echo "Testar http://test-ipv6.se/ eller http://ipv6.test-ipv6.se/"
#echo curl --progress-bar --ipv4 --url http://ipv6.test-ipv6.se/
#curl --progress-bar --ipv4 --url http://test-ipv6.se/

echo -e "Testa alla med länken http://test-ipv6.se/mirrors.html#"

exit 0
# eof

