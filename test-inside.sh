#!/bin/bash
#
# Do some tests if a Machine is set up as a server.
# Check if using IPv6 tunnel for a lan for HE.
# Works with native IPv6 too.
#
# Todo: Kolla om getent 127.0.0.0 ger konstigt, eventuellt
#       använd grep på /etc/hosts istället för getent hosts.
#
# Date      Name            What
# -------- ---------------- ---------------
# 2016xxyy Anders Jackson   First version
# 20170125 Anders Jackson   Some clean up
# 20190114 Anders Jackson   Fix a bug with OWNIPv6REC -> OWNIPv6REV (and for IPv4)
# 20190214 Anders Jackson   Fixar test om kommandon existerar.
# 20190327 Anders Jackson   Stavfel och annat smått och gott.

if [ $# -ne 2 ]; then 
  echo -e 2>1 "usage: `basename $0` device IPv6-address"
  echo -e 2>1 "   device: the device that is the LAN (see ip(1))"
  echo -e 2>1 "   IPv6-address: like dns.google.com to test connections against"
  echo -e 2>1 "\n       Device | IP address"

  # If this doesn't work, install gawk.
  ip -o add show | \
  awk -e "/([[:xdigit:]]{0,4}:){1,7}[[:xdigit:]]{1,4}\/[[:digit:]]{1,3}/\
            { printf \"     %8s | %s \\n\", \$2, \$4 }"\
      -e "/([[:digit:]]{1,3}\.?){4}\/[[:digit:]]{1,2}/\
            { printf \"     %8s | %s\\n\", \$2, \$4 }" 2>1

  exit 1
fi

DEVICE="$1"
SERVER="$2"

# TODO: Fixa så kontrollerar IPv6 och inte IPv4
# TODO: Och att uppslag på ickeexisterande även stoppar
echo -e "\nSlå upp i resursen \"hosts\" efter \"$SERVER\""
echo getent hosts $SERVER

if [ ! -z "`getent hosts $SERVER`" ] ; then 
    getent hosts $SERVER
else
    echo -e "getent hosts $SERVER fungerar inte"
    exit 1
fi

echo -e "\n       Device | IP address"
ip -o add show | \
awk -e "/(:[[:xdigit:]]{0,4}:){1,7}[[:xdigit:]]{1,4}\/[[:digit:]]{1,3}/\
          { printf \"     %8s | %s \\n\", \$2, \$4 }"\
    -e "/([[:digit:]]{1,3}\.?){4}\/[[:digit:]]{1,2}/\
          { printf \"     %8s | %s\\n\", \$2, \$4 }"

echo -e "\nRouting tabel för $DEVICE"
ip -6 route list dev $DEVICE

# Device -> IPv4/Netmask
# ip address show dev $DEVICE scope global | \
#    grep -o -e "\([[:digit:]]\{1,3\}\.\?\)\{4\}/[[:digit:]]\{1,2\}"
#
# Device -> IPv4
# ip address show dev $DEVICE scope global | \
#    grep -o -e "\([[:digit:]]\{1,3\}\.\?\)\{4\}/[[:digit:]]\{1,2\}" |\
#    cut -d\ -f1
#
# Device -> IP4 Name (looked up in /e/hosts or DNS)
# getent hosts `ip address show dev $DEVICE scope global | \
#     grep -o -e "\([[:digit:]]\{1,3\}\.\?\)\{4\}/[[:digit:]]\{1,2\}" | \
#     sed "s|/[[:digit:]]\{1,2\}||"`
# getent hosts `ip address show dev $DEVICE scope global | \
#     grep -o -e "\([[:digit:]]\{1,3\}\.\?\)\{4\}/[[:digit:]]\{1,2\}" | \
#     cut -d\ -f1` 
#

OWNIPv6=$(ip -o -6 add list dev $DEVICE scope global | \
          grep -m1 -oe "2[[:xdigit:]]\{3\}:\?\(:[[:xdigit:]]\{0,4\}\)\{1,7\}")
OWNIPv4=$(ip -o -4 address show dev $DEVICE scope global | \
          grep -m1 -oe '[[:digit:]]\{1,3\}\(\.[[:digit:]]\{1,3\}\)\{3\}/[[:digit:]]\{1,2\}' | \
          grep -oe '^[[:digit:]]\{1,3\}\(.[[:digit:]]\{1,3\}\)\{3\}')

echo -e "\nEgna IPv6: $OWNIPv6"
echo -e "Egna IPv4: $OWNIPv4"

if [ "$(getent hosts 127.0.1.1)" != "" ] ; then
    echo -e 2>1 "Adress 127.0.1.1 skall inte finnas i en server."
    echo -e 2>1 " det är ett hack för mobila maskiner."
    echo -e 2>1 "Servrar BÖR ha statiska adresser för extern access"
    echo -e 2>1 "Ändra i /etc/hosts så att maskinens IPv4-adress är den statiska (samma för IPv6)"
else
    echo -e "\nAdressen 127.0.1.1 är fixat, bra!"
    echo -e "\n(möjligen buggig detektering i nyare Debian)"
fi

OWNIPv6REV=$(getent hosts "$OWNIPv6" | grep -m1 -oe "$HOSTNAME$")
OWNIPv4REV=$(getent hosts "$OWNIPv4" | grep -m1 -oe "$HOSTNAME$")

if [ "$OWNIPv6REV" != $HOSTNAME -o "$OWNIPv4REV" != $HOSTNAME ] ; then
    echo -e 2>1 "\nFel vid uppslag av $HOSTNAME"
    echo -e 2>1 "Inte samma resultat för IPv6 och IPv4."
    getent hosts $OWNIPv6 2>1
    getent hosts $OWNIPv4 2>1
else
    echo -e "\nMaskinens båda IPv6 och IPv4-adresser slås upp till samma, bra!"
fi

echo -e "\nTest ICMP6 ECHO"
echo ping6 -c 3 $SERVER
ping6 -c 3 $SERVER

echo -e "\nTest routing IPv6 (at lease one should exist)"
hopp=29
if [ ! -z `which traceroute6` ]; then
    echo "traceroute6 -m $hopp -p 80 $SERVER"
    traceroute6 -m $hopp -p 80 $SERVER
else
    echo -e "Inte installerat: traceroute6"
fi

if [ ! -z `which tracepath` ]; then
    echo "tracepath -6 -b -m $hopp -p 80 $SERVER"
    tracepath -6 -b -m $hopp -p 80 $SERVER
else
    echo -e "Inte installerat: tracepath"
fi

if [ ! -z `which rltraceroute6` ]; then
    echo "rltraceroute6 -m $hopp $SERVER"
    rltraceroute6 -m $hopp $SERVER
else
    echo -e "Inte installerat: rltraceroute6"
fi

if [ ! -z `which tcptraceroute6` ]; then
    echo "tcptraceroute6 -m $hopp $SERVER"
    tcptraceroute6 -m $hopp $SERVER
else
    echo -e "Inte installerat: tcptraceroute6"
fi

echo -e "\nTest open ports (nmap)"
echo nmap -AT4 $OWNIPv4
nmap -AT4 $OWNIPv4
echo -e "\n"
echo nmap -6 -AT4 $OWNIPv6
nmap -6 -AT4 $OWNIPv6

# Lokalt, innefrån
#echo 
#echo "Testar http://test-ipv6.se/ eller http://ipv6.test-ipv6.se/"
#echo curl --progress-bar --ipv4 --url http://ipv6.test-ipv6.se/
#curl --progress-bar --ipv4 --url http://test-ipv6.se/
#echo "Testa alla med länken http://test-ipv6.se/mirrors.html#"
#TESTIPv4=$(curl -f -4 --url http://ipv4.test-ipv6.se/images/knob_valid_green.png | \
#           grep -c -e "knob_valid_green.png")

echo -e "\n"
curl -sf  --url http://ds.test-ipv6.se/images/knob_valid_green.png -o /dev/null
TESTIP=$?
curl -sf4 --url http://ipv4.test-ipv6.se/images/knob_valid_green.png -o /dev/null
TESTIPv4=$?
curl -sf6 --url http://ipv6.test-ipv6.se/images/knob_valid_green.png -o /dev/null
TESTIPv6=$?

if [ "$TESTIP" -ne 0 -o "$TESTIPv4" -ne 0 -o "$TESTIPv6" -ne 0 ]; then
    echo -e 2>1 "\nTest mot http://test-ipv6.se/simple_test.html fugerar inte."
    echo -e 2>1 "Prova med en webbläsare mot http://test-ipv6.se/"
    echo -e 2>1 "Best of both:$TESTIP IPv4:$TESTIPv4 IPv6:$TESTIPv6"
else
    echo -e "\nLänken http://test-ipv6.se/ verkar fungera."
    echo -e "Så antar att IPv4 och IPv6 fungear."
fi

exit 0
# eof

