# Test_net

Testing connectivity within and from the outside of a home site
running IPv6.

## Command test-inside.sh

Test a Linux host set up as a IPv6 tunnel or IPv6 connection. It also
check some basic configurations in the server.

Run it with two arguments, which will be printed if you do not enter
them.

First argument is the device to use, the second is the IPv6
machine/address to test against.

## Command test-outside.sh

Test connections to a IPv6 machine from external network.

Run it with one argument, which also will be printed if you do not
enter them.

First argument is the IPv6 machine name/address to test the connection
against.

